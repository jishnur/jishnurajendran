(TeX-add-style-hook
 "publications"
 (lambda ()
   (LaTeX-add-bibitems
    "Rajendran_2018"
    "Rajendran2018"
    "Ridolfo2021"
    "Pope2019"
    "Giannelli2022"
    "jishnu_lattice"
    "jabref-meta:"))
 :bibtex)

